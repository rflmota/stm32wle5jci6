/*
 * Manchester.h
 */

#ifndef MANCHESTER_H_
#define MANCHESTER_H_

#include "main.h"
#include <stdint.h>

typedef uint8_t byte;

void processTag(byte* RFIDtagArray, char* RFIDstring, byte RFIDtagUser,
				unsigned long* RFIDtagNumber);
byte FastRead(byte whichCircuit, byte checkDelay, unsigned int readTime);
void INT_demodOut();
void shutDownRFID();

#endif
